package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

//    @PreAuthorize("hasAuthority('get')")
//    @GetMapping(value = "")
//    public ResponseEntity<List<TurnoDto>> findAll() {
//        return ResponseEntity
//                .ok(turnoService.findAllTurnos())
//                ;
//    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<TurnoDto>> findAllByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "id") String orderField
    ) {

        return ResponseEntity.ok(turnoService.findAllByPage(pageNumber,pageSize,orderField));

    }

//    @PreAuthorize("hasAuthority('get')")
//    @GetMapping(value = "/{id}")
//    public ResponseEntity<TurnoDto> findOne(@PathVariable(name = "id") Integer id) {
//        return ResponseEntity
//                .ok(
//                        turnoService.findTurnoById(id));
//    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Page<TurnoDto>> findOne(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(
                        turnoService.findTurnoById(id));
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> addTurno(@RequestBody @Validated TurnoDto dto){
        return ResponseEntity.ok(turnoService.addTurno(dto));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> editTurno(@PathVariable(name = "idTurno") int id,
                                                    @RequestBody TurnoDto dto) {
        return ResponseEntity.ok(turnoService.editTurno(id, dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> deletePaciente(@PathVariable(name = "idTurno") int id) {
        return ResponseEntity.ok(turnoService.deleteTurno(id));
    }
}
