package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;

    }

//    @PreAuthorize("hasAuthority('get')")
//    @GetMapping(value = "")
//    public ResponseEntity<List<PacienteDto>> findAll() {
//        return ResponseEntity
//                .ok(pacienteService.findAllPacientes())
//                ;
//    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<PacienteDto>> findAllByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "id") String orderField
    ) {

        return ResponseEntity.ok(pacienteService.findAllByPage(pageNumber,pageSize,orderField));

    }

//    @PreAuthorize("hasAuthority('get')")
//    @GetMapping(value = "/{id}")
//    public ResponseEntity<PacienteDto> findOne(@PathVariable(name = "id") Integer id) {
//        return ResponseEntity
//                .ok(
//                        pacienteService.findPacienteById(id))
//                ;
//    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Page<PacienteDto>> findOne(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(
                        pacienteService.findPacienteById(id))
                ;
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<PacienteDto> addPaciente(@RequestBody @Validated PacienteDto dto){

        return ResponseEntity.ok(pacienteService.addPaciente(dto));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteDto> editPaciente(@PathVariable(name = "idPaciente") int id,
                                                @RequestBody PacienteDto dto) {

        return ResponseEntity.ok(pacienteService.editPaciente(id, dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> deletePaciente(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.deletePaciente(id));
        //return ResponseEntity.ok(personaService.deletePersona(pacienteService.findPacienteById(id).getDni()));
    }
}
