package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/medico")
public class MedicoController {

    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;

    }

//    @PreAuthorize("hasAuthority('get')")
//    @GetMapping(value = "")
//    public ResponseEntity<List<MedicoDto>> findAll() {
//        return ResponseEntity
//                .ok(medicoService.findAllMedicos())
//                ;
//    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<MedicoDto>> findAllByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "apellido") String orderField
    ) {

        return ResponseEntity.ok(medicoService.findAllByPage(pageNumber,pageSize,orderField));

    }

//    @PreAuthorize("hasAuthority('get')")
//    @GetMapping(value = "/{id}")
//    public ResponseEntity<MedicoDto> findOne(@PathVariable(name = "id") Integer id) {
//        return ResponseEntity
//                .ok(
//                        medicoService.findMedicoById(id))
//                ;
//    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Page<MedicoDto>> findOne(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(
                        medicoService.findMedicoById(id))
                ;
    }


    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> addMedico(@RequestBody @Validated MedicoDto dto){

        return ResponseEntity.ok(medicoService.addMedico(dto));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> editMedico(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody MedicoDto dto) {

        return ResponseEntity.ok(medicoService.editMedico(id, dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> deleteMedico(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.deleteMedico(id));
        //return ResponseEntity.ok(personaService.deletePersona(medicoService.findMedicoById(id).getDni()));
    }



}
