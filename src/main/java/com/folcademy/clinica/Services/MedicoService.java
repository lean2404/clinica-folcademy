package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service("medicoService")
public class MedicoService implements IMedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final PersonaMapper personaMapper;
    private final PersonaRepository personaRepository;

    public MedicoService(MedicoMapper medicoMapper, MedicoRepository medicoRepository, PersonaMapper personaMapper, PersonaRepository personaRepository) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaMapper = personaMapper;
        this.personaRepository = personaRepository;
    }


    public List<MedicoDto> findAllMedicos() {

        List<Medico> medicos = (List<Medico>) medicoRepository.findAll();
        return medicos.stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
//        if(medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList()).size() > 0)
//            return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());

        //throw new NotFoundException("Buscar fallido: no hay medicos cargados");
    }


//    public MedicoDto findMedicoById(Integer id) {
//        if(!medicoRepository.existsById(id))
//            throw new NotFoundException("Buscar fallido: Id "+ id +" no existe");
//        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
//    }



    public MedicoDto addMedico(MedicoDto dto){
        if(dto.getConsulta()<0)
            throw new BadRequestException("Agregar fallido: La consulta no puede ser menor a 0");
        dto.setId(null);

        PersonaDto personaDto = new PersonaDto(dto.getDni(), dto.getNombre(), dto.getApellido(), dto.getTelefono());
        personaRepository.save(personaMapper.dtoToEntity(personaDto));

        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(dto)));
    }  //POST


    public MedicoDto editMedico(Integer id, MedicoDto dto){
        if(id<0)
            throw new BadRequestException("Editar fallido: Id no puede ser menor a 0");
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("Editar fallido: Id "+ id +" no existe");

        dto.setId(id);

        PersonaDto personaDto = new PersonaDto(dto.getDni(), dto.getNombre(), dto.getApellido(), dto.getTelefono());
        personaRepository.save(personaMapper.dtoToEntity(personaDto));

        return medicoMapper.entityToDto(
                medicoRepository.save(
                        medicoMapper.dtoToEntity(dto)
                )
        );
    } //PUT


    public Boolean deleteMedico(Integer id){
        if(id<0)
            throw new BadRequestException("Eliminar fallido: Id no puede ser menor a 0");
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("Eliminar fallido: Id "+ id +" no existe");

        medicoRepository.deleteById(id);
        return true;

    } //DELETE

    public Page<MedicoDto> findAllByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToDto);
    }

    public Page<MedicoDto> findMedicoById(Integer id) {
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("Buscar fallido: Id "+ id +" no existe");
        Pageable pageable = PageRequest.of(0,1, Sort.by("id"));
        return medicoRepository.findById(id, pageable).map(medicoMapper::entityToDto);
    }
}
