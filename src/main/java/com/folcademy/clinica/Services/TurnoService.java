package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("turnoService")
public class TurnoService implements ITurnoService {

    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }



    public List<TurnoDto> findAllTurnos() {

        List<Turno> turno = (List<Turno>) turnoRepository.findAll();
        if((long) turno.size() > 0){
            return turno.stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
        }
        throw new NotFoundException("Buscar fallido: no hay turnos cargados");
    }


//    public TurnoDto findTurnoById(Integer id) {
//        if(!turnoRepository.existsById(id))
//            throw new NotFoundException("Buscar fallido: Id "+ id +" no existe");
//        return turnoRepository.findById(id).map(turnoMapper::entityToDto).orElse(null);
//    }


    public TurnoDto addTurno(TurnoDto dto){
        if(dto.getMedico().getId()  < 0)
            throw new BadRequestException("Agregar fallido: El Id del medico no puede ser menor a 0");
        if(dto.getPaciente().getId() < 0)
            throw new BadRequestException("Agregar fallido: El id no puede ser menor a 0");
        dto.setIdturno(null);
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
    }  //POST


    public TurnoDto editTurno(Integer id, TurnoDto dto){
        if(id<0)
            throw new BadRequestException("Editar fallido: Id no puede ser menor a 0");
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("Editar fallido: Id "+ id +" no existe");

        dto.setIdturno(id);
        return turnoMapper.entityToDto(
                turnoRepository.save(
                        turnoMapper.dtoToEntity(dto)
                )
        );
    } //PUT


    public Boolean deleteTurno(Integer id){
        if(id<0)
            throw new BadRequestException("Eliminar fallido: Id no puede ser menor a 0");
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("Eliminar fallido: Id "+ id +" no existe");

        turnoRepository.deleteById(id);
        return true;

    } //DELETE

    public Page<TurnoDto> findAllByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }

    public Page<TurnoDto> findTurnoById(Integer id) {
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("Buscar fallido: Id "+ id +" no existe");

        Pageable pageable = PageRequest.of(0,1, Sort.by("id"));
        return turnoRepository.findById(id, pageable).map(turnoMapper::entityToDto);
    }


}
