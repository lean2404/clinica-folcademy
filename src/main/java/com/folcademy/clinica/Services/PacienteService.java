package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaMapper personaMapper;
    private final PersonaRepository personaRepository;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }



    public List<PacienteDto> findAllPacientes() {
//        if(pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList()).size() > 0)
//            return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
//        throw new NotFoundException("Buscar fallido: no hay pacientes cargados");

        List<Paciente> pacientes = (List<Paciente>) pacienteRepository.findAll();
        return pacientes.stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }


//    public PacienteDto findPacienteById(Integer id) {
//        if(!pacienteRepository.existsById(id))
//            throw new NotFoundException("Buscar fallido: Id "+ id +" no existe");
//        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
//    }


    public PacienteDto addPaciente(PacienteDto dto){

        if(dto.getDni()  < 0)
            throw new BadRequestException("Agregar fallido: El dni no puede ser menor a 0");
        if(Integer.parseInt(dto.getTelefono()) < 0)
            throw new BadRequestException("Agregar fallido: El telefono no puede ser menor a 0");
        dto.setId(null);

        PersonaDto personaDto = new PersonaDto(dto.getDni(), dto.getNombre(), dto.getApellido(), dto.getTelefono());
        personaRepository.save(personaMapper.dtoToEntity(personaDto));

        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(dto)));
    }  //POST


    public PacienteDto editPaciente(Integer id, PacienteDto dto){
        if(id<0)
            throw new BadRequestException("Editar fallido: Id no puede ser menor a 0");
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Editar fallido: Id "+ id +" no existe");

        dto.setId(id);

        PersonaDto personaDto = new PersonaDto(dto.getDni(), dto.getNombre(), dto.getApellido(), dto.getTelefono());
        personaRepository.save(personaMapper.dtoToEntity(personaDto));

        return pacienteMapper.entityToDto(
                pacienteRepository.save(
                        pacienteMapper.dtoToEntity(dto)
                )
        );
    } //PUT


    public Boolean deletePaciente(Integer id){
        if(id<0)
            throw new BadRequestException("Eliminar fallido: Id no puede ser menor a 0");
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Eliminar fallido: Id "+ id +" no existe");

        pacienteRepository.deleteById(id);
        return true;

    } //DELETE

    public Page<PacienteDto> findAllByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }

    public Page<PacienteDto> findPacienteById(Integer id) {
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Buscar fallido: Id "+ id +" no existe");

        Pageable pageable = PageRequest.of(0,1, Sort.by("id"));
        return pacienteRepository.findById(id, pageable).map(pacienteMapper::entityToDto);
    }
}
