package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;

import java.util.List;
import java.util.Optional;

public interface ITurnoService {


    List<TurnoDto> findAllTurnos();
    //TurnoDto findTurnoById(Integer id);
    TurnoDto addTurno(TurnoDto dto);  //POST
    TurnoDto editTurno(Integer id, TurnoDto dto); //PUT
    Boolean deleteTurno(Integer id); //DELETE

}
