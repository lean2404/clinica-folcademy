package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;

import java.util.List;

public interface IMedicoService {

    List<MedicoDto> findAllMedicos();
    //MedicoDto findMedicoById(Integer id);
    MedicoDto addMedico(MedicoDto dto);  //POST
    MedicoDto editMedico(Integer id, MedicoDto dto); //PUT
    Boolean deleteMedico(Integer id); //DELETE

}
