package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.PacienteDto;

import java.util.List;

public interface IPacienteService {

    List<PacienteDto> findAllPacientes();
    //PacienteDto findPacienteById(Integer id);
    PacienteDto addPaciente(PacienteDto dto);  //POST
    PacienteDto editPaciente(Integer id, PacienteDto dto); //PUT
    Boolean deletePaciente(Integer id); //DELETE


}
