package com.folcademy.clinica.Model.Dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {
    Integer idturno;
    LocalDate fecha = LocalDate.now();
    LocalTime hora = LocalTime.now();
    Boolean atendido;
    PacienteDto paciente;
    MedicoDto medico;

}
