package com.folcademy.clinica.Model.Dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class MedicoDto{
    Integer id;
    Integer dni;
    String nombre = "";
    String apellido = "";
    String telefono = "";
    String profesion = "";
    int consulta = 0;

}
