package com.folcademy.clinica.Model.Entities;


import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
        Integer id;
        @Column(name = "profesion", columnDefinition = "VARCHAR")
        String profesion;
        @Column(name = "consulta", columnDefinition = "VARCHAR")
        int consulta;
        @Column(name = "dni", columnDefinition = "INT")
        Integer dni;

        @OneToOne
        @NotFound(action = NotFoundAction.IGNORE)
        @JoinColumn(name = "dni", referencedColumnName = "dni", insertable = false, updatable = false)
        private Persona persona;

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
                Medico medico = (Medico) o;
                return Objects.equals(id, medico.id);
        }

        @Override
        public int hashCode() {
                return getClass().hashCode();
        }
}