package com.folcademy.clinica.Model.Entities;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "persona")
public class Persona {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dni", columnDefinition = "INT(10)")
    Integer dni;
    @Column(name = "nombre", columnDefinition = "VARCHAR")
    String nombre;
    @Column(name = "apellido", columnDefinition = "VARCHAR")
    String apellido;
    @Column(name = "telefono", columnDefinition = "VARCHAR")
    String telefono;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Persona persona = (Persona) o;
        return Objects.equals(dni, persona.dni);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
