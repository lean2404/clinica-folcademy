package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class PacienteMapper {

    private final PersonaMapper personaMapper = new PersonaMapper();

    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                personaMapper.entityToDto(ent.getPersona()).getDni(),
                                personaMapper.entityToDto(ent.getPersona()).getNombre(),//ent.getNombre(),
                                personaMapper.entityToDto(ent.getPersona()).getApellido(),
                                personaMapper.entityToDto(ent.getPersona()).getTelefono(),
                                ent.getDireccion()
                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();

        PersonaDto p = new PersonaDto(dto.getDni(), dto.getNombre(), dto.getApellido(), dto.getTelefono());
        entity.setPersona(personaMapper.dtoToEntity(p));

        entity.setId(dto.getId());
        entity.setDni(dto.getDni());
        entity.setDireccion(dto.getDireccion());


        return entity;
    }


}
