package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {

    private final MedicoMapper medicoMapper = new MedicoMapper();
    private final PacienteMapper pacienteMapper = new PacienteMapper();


    public TurnoDto entityToDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                pacienteMapper.entityToDto(ent.getPaciente()),
                                medicoMapper.entityToDto(ent.getMedico())
                        )
                )
                .orElse(new TurnoDto());
    }

    public Turno dtoToEntity(TurnoDto dto){
        Turno entity = new Turno();
        entity.setId(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getPaciente().getId());
        entity.setIdmedico(dto.getMedico().getId());
        entity.setMedico(medicoMapper.dtoToEntity(dto.getMedico()));
        entity.setPaciente(pacienteMapper.dtoToEntity(dto.getPaciente()));
        return entity;
    }


}
