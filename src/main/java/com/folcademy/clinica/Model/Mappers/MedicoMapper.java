package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class MedicoMapper {

    private final PersonaMapper personaMapper = new PersonaMapper();

    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getId(),
                                personaMapper.entityToDto(ent.getPersona()).getDni(),
                                personaMapper.entityToDto(ent.getPersona()).getNombre(),//ent.getNombre(),
                                personaMapper.entityToDto(ent.getPersona()).getApellido(),
                                personaMapper.entityToDto(ent.getPersona()).getTelefono(),
                                ent.getProfesion(),
                                ent.getConsulta()
                        )
                )
                .orElse(new MedicoDto());
    }

    public Medico dtoToEntity(MedicoDto dto){
        Medico entity = new Medico();

        PersonaDto p = new PersonaDto(dto.getDni(), dto.getNombre(), dto.getApellido(), dto.getTelefono());
        entity.setPersona(personaMapper.dtoToEntity(p));

        entity.setId(dto.getId());
        entity.setDni(dto.getDni());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());

        return entity;
    }
}
